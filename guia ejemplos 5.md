# OBJETIVO
Realizar una aplicacion para operar con dos numeros

# PASOS

- crear archivo html
- crear archivo js
- en el archivo html coloco la estructura basica (!)
- detras del body colocar la etiqueta <script src=""></script> (sc)
- en el html coloco el formulario <form></form>(form)
- coloco un div para cada control
- en el primer div coloco label + input
- dentro del label colocamos el Texto a mostrar en pantalla
- en la etiqueta input colocamos un atributo id con el nombre que queramos colocar al control
- en el atributo for de la etiqueta label colocamos el mismo valor que en el atributo id de la etiqueta input
- colocamos en el tercer div la etiqueta button
- dentro de la etiqueta button colocamos el texto a mostrar en el boton (multiplicar)

- ahora empieza la programacion en js
- en la etiqueta button añadimos un atributo type con valor button
- en la etiqueta button añadimos un atributo onclick donde colocamos como valor la funcion de javascript que se va a llamar para realizar la operacion
- en el archivo js creamos las funciones que hemos colocado en el onclick
- Ahora voy a trabajar en la funcion multiplicar
- creo una variable para almacenar el resultado
- creo una variable para apuntar a cada caja de texto

~~~js
let numero1 = document.querySelector("#numero1");
let numero2 = document.querySelector("#numero2");
~~~

- colocado en la variable resultado, la multiplicacion de los dos numeros

~~~js
resultado = numero1.value * numero2.value;
~~~

- muestro el resultado. En este ejercicio lo voy a realizar con un mensaje emergente (alert)

~~~js
alert(resultado);
~~~




