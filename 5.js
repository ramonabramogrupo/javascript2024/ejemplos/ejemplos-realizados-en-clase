function multiplicar() {
    // esta variable es para almacenar el resultado
    let resultado = 0;
    // esta variable apunta a la primera caja de texto
    let numero1 = document.querySelector("#numero1");
    // esta variable apunta a la segunda caja de texto    
    let numero2 = document.querySelector("#numero2");

    // ahora es el procesamiento
    resultado = numero1.value * numero2.value;

    // ahora la salida
    alert(resultado);



}

function restar() {
    // esta variable es para almacenar el resultado
    let resultado = 0;
    // esta variable apunta a la primera caja de texto
    let numero1 = document.querySelector("#numero1");
    // esta variable apunta a la segunda caja de texto    
    let numero2 = document.querySelector("#numero2");

    // ahora es el procesamiento
    resultado = numero1.value - numero2.value;

    // ahora la salida
    alert(resultado);
}