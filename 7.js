function mayor() {
    // crear variables
    // leo el numero escrito en la caja numero1 y lo convierto a numero1
    let numero1 = parseFloat(document.querySelector("#numero1").value);

    // leo el numero escrito en la caja numero2 y lo convierto a numero2
    let numero2 = parseFloat(document.querySelector("#numero2").value);

    // variable para almacenar el numero mas grande
    let resultado = 0;

    // procesamiento
    if (numero1 > numero2) {
        resultado = "el mayor es " + numero1;
    } else {
        if (numero1 == numero2) {
            resultado = "los dos son iguales"
        } else {
            resultado = "el mayor es " + numero2;
        }

    }

    // salida
    alert(resultado);

}
