//document.write("Ramon");

// variable apunta a la caja1
let caja1 = document.querySelector("#caja1");
let caja2 = document.querySelector("#caja2");

// prompt para pedir dos numeros
let numero1 = prompt("Ingrese un numero");
let numero2 = prompt("Ingrese otro numero");

// para hacer pruebas
// let numero1 = "4";
// let numero2 = "5";

// producto de los dos numeros
let resultado = numero1 * numero2;

// escribir en la caja1 los dos numeros

// opcion1
// en una sola linea
caja1.innerHTML = numero1 + "<br/>" + numero2;

// opcion2
// esto es lo mismo pero en varias lineas
// caja1.innerHTML = numero1;
// caja1.innerHTML += "<br/>";
// caja1.innerHTML += numero2;

// cambiar el diseño de la caja1
caja1.style.color = "white";
caja1.style.backgroundColor = "black";
caja1.style.padding = "10px";
caja1.style.width = "100px";

// cambiar el diseño de la caja2
caja2.style.color = "white";
caja2.style.backgroundColor = "black";
caja2.style.padding = "40px";
caja2.style.width = "100px";
caja2.style.margin = "10px auto"; // centrar

// quiero dibujar el producto de los dos numeros en la caja2
caja2.innerHTML = resultado;

