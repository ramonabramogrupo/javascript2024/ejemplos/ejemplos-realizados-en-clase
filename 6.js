function leer() {
    // variables para leer los datos

    // nombre apunta a la caja de texto nombre
    let nombre = document.querySelector("#nombre");

    // variable para escribir los resultados

    // resultado apunta a la caja de texto resultado
    let resultado = document.querySelector("#resultado");

    // operaciones
    // quiero pasar lo que esta en nombre a resultado

    resultado.value = nombre.value;

}